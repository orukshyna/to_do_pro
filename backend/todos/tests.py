from django.test import TestCase
from faker import Faker

from .models import Todo


class TodoModelTest(TestCase):
    faker = Faker()
    TITLE = f'Test title for todo "{faker.word()}"'
    BODY = f'Test body for todo "{faker.word()}"'

    @classmethod
    def setUpTestData(cls):
        Todo.objects.create(title=cls.TITLE, body=cls.BODY)

    def test_title_content(self):
        todo_id = Todo.objects.latest('id')
        expected_title = f'{todo_id.title}'
        self.assertEqual(expected_title, self.TITLE)

    def test_body_content(self):
        todo_id = Todo.objects.latest('id')
        expected_body = f'{todo_id.body}'
        self.assertEqual(expected_body, self.BODY)
