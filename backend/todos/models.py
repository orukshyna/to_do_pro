from django.db import models


class Todo(models.Model):
    title = models.CharField(max_length=100)
    body = models.TextField(max_length=500, default='Default text')

    def __str__(self):
        return self.title
