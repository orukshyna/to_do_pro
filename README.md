## _**ToDoPro**_  🧭

## 👋 intro 📃

ToDo project contains 2 parts:
- backend (DRF);
- frontend (React).


### Setup and run 
```
git clone https://gitlab.com/orukshyna/to_do_pro.git
```
* activate venv;
* check that rest_framework and cors are installed for backend;
* check that nodejs and axios are installed for frontend;
* from backend run:
```
python manage.py runserver
```
* from frontend run:
```
npm start
```

### Verify the projects in browser

* for backend navigate to
```
http://127.0.0.1:5000/api/ 
```

![img.png](img.png)

* for frontend navigate to
```
http://localhost:3000/
```
![img_1.png](img_1.png)

_Appreciate_ the author of the book. The book which inspired me to build the project:
Django for APIs William S. Vincent https://djangoforapis.com/  👏
